#ifndef MAP_H
#define MAP_H

#include <iostream>
#include <string>

class Map{

	private:
		char maze[20][50];
		char maze_new[20][50];
		char gameOver[20][50];
		char win[20][50];
		char welcome[20][50];

	public:
		Map();

		void setMaze();
		void getMaze();
		void cleanMaze();
		void addElemento(char sprite, int posx, int posy);
		void GameOver();
		void Win();
		void Welcome();

		char retornaElemento(int posx, int posy);

};

#endif

