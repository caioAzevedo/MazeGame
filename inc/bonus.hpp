#ifndef BONUS_H
#define BONUS_H

#include <iostream>
#include <string>
#include "gameObject.hpp"

class Bonus : public gameObject {

	private:
		char character;
		int posx;
		int posy;
    int score;


	public:
		Bonus();

		void setPosx(int valor);
		void setPosy(int valor);
		void setCharacter(char character);
    void setScore(int score);

		int getPosx();
		int getPosy();
		char getCharacter();
    int getScore();

		void movimento();

};

#endif
