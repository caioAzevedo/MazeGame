#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <iostream>
#include <string>

class gameObject{

	private:
		char character;
		int posx;
		int posy;

	public:
		gameObject();
		gameObject(char character, int posx, int posy);

		void setPosx(int valor);
		void setPosy(int valor);
		void setCharacter(char character);
		int getPosx();
		int getPosy();
		char getCharacter();
		void movimento();

};

#endif

