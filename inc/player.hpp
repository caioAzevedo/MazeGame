#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <string>
#include "gameObject.hpp"

class Player : public gameObject {

	private:
		char character;
		int posx;
		int posy;
		int alive;
		int score;
		bool winner;

	public:
		Player();
		

		void setPosx(int valor);
		void setPosy(int valor);
		void setCharacter(char character);
		void setAlive();
		void setScore(int valor);
		void setWinner(bool win);

		int getPosx();
		int getPosy();
		char getCharacter();
		int getAlive();
		int getScore();
		bool getWinner();

		void movimento(char w, char s, char a, char d);
		

};

#endif
