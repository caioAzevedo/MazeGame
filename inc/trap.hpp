#ifndef TRAP_H
#define TRAP_H

#include <iostream>
#include <string>
#include "gameObject.hpp"

class Trap : public gameObject {

	private:
		char character;
		int posx;
		int posy;
    int damage;


	public:
		Trap();

		void setPosx(int valor);
		void setPosy(int valor);
		void setCharacter(char character);
    void setDamage(int damage);

		int getPosx();
		int getPosy();
		char getCharacter();
    int getDamage();

		void movimento();

};

#endif

