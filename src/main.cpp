#include <iostream>
#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <stdio_ext.h>
#include "map.hpp"
#include "player.hpp"
#include "trap.hpp"
#include "bonus.hpp"

using namespace std;



int main(int argc, char **argv){

	Map * mapa = new Map();

	mapa->setMaze();

	Player * player = new Player();
	Trap * trap[20];
	for(int i = 0; i < 20; i++){
		trap[i] = new Trap();
		trap[i]-> movimento();
		while(mapa->retornaElemento(trap[i]->getPosy(), trap[i]->getPosx()) == '='){
			trap[i]-> movimento();
		}
	}
	Bonus * bonus[5];
	for(int i = 0; i < 5; i++){
		bonus[i] = new Bonus();
		bonus[i]-> movimento();
		while(mapa->retornaElemento(bonus[i]->getPosy(), bonus[i]->getPosx()) == '='){
			bonus[i]-> movimento();
		}
	}

	int tempo = 0;
	char key_press;
	
	initscr();
	clear();
	
	mapa->Welcome();
		key_press = getch();
	        refresh();


	while(TRUE){
		initscr();
		clear();
		keypad(stdscr, TRUE);
		noecho();

		for(int i = 0; i < 20; i++){
			mapa->addElemento(trap[i]->getCharacter(), trap[i]->getPosx(), trap[i]->getPosy());
			if(tempo == 4){
				trap[i]->movimento();
				while(mapa->retornaElemento(trap[i]->getPosy(), trap[i]->getPosx()) == '='){
					trap[i]-> movimento();
				}
			}
		}

		for(int i = 0; i < 5; i++){
			mapa->addElemento(bonus[i]->getCharacter(), bonus[i]->getPosx(), bonus[i]->getPosy());
			if(tempo == 4 ){
				bonus[i]->movimento();
				while(mapa->retornaElemento(bonus[i]->getPosy(), bonus[i]->getPosx()) == '='){
					bonus[i]-> movimento();
				}
				tempo = 0;
			}
		}

		mapa->addElemento(player->getCharacter(), player->getPosx(), player->getPosy());
		mapa->getMaze();
		tempo++;

		printw("Player = %c, life = %d, score = %d, %c", player->getCharacter(), player->getAlive(), player->getScore(), mapa->retornaElemento(player->getPosy(),player->getPosx()));
		printw("\nMOVE: (W) (A) (S) (D)");

		player->movimento(mapa->retornaElemento(player->getPosy() -1, player->getPosx()),
		mapa->retornaElemento(player->getPosy() +1, player->getPosx()),
		mapa->retornaElemento(player->getPosy(), player->getPosx() -1),
		mapa->retornaElemento(player->getPosy(), player->getPosx() +1));

		for(int i = 0; i < 20; i++){
			if(player->getPosx() == trap[i]->getPosx() && player->getPosy() == trap[i]->getPosy()){
				player->setAlive();
			}
		}

		if(player->getAlive() == 0){
			clear();
			mapa->GameOver();
			key_press = getch();
			endwin();
			break;
		}

		for(int i = 0; i < 5 ;i++){
			if(player->getPosx() == bonus[i]->getPosx() && player->getPosy() == bonus[i]->getPosy()){
				player->setScore(10);
			}
		}

		if(player->getPosx() == 49 && player->getPosy() == 16){
			clear();
			mapa->Win();
			key_press = getch();
			endwin();
			break;
		}

		mapa->cleanMaze();

		refresh();
		endwin();
	}

	delete(mapa);
	delete(player);

	return 0;
}

