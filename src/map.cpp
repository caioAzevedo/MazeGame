#include <iostream>
#include <string>
#include "map.hpp"
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>

using namespace std;

Map::Map(){}

void Map::setMaze(){

	ifstream map ("map.txt");

	string aux;

	for(int i = 0; i < 20; i++){
		getline(map, aux);
		for(int j = 0; j < 50; j++){
			this->maze[i][j] = aux[j];
			this->maze_new[i][j] = aux[j];
		}
	}

	map.close();
}

void Map::getMaze(){

		for(int i = 0; i < 20; i++){
			for(int j = 0; j < 50; j++){
				printw("%c", this->maze[i][j]);
			}
		printw("\n");
		}
}

void Map::cleanMaze(){

	for(int i = 0; i < 20; i++){
		for(int j = 0; j < 50; j++){
			this->maze[i][j] = this->maze_new[i][j];
		}
	}
}

void Map::addElemento(char character, int posx, int posy){

	this->maze[posy][posx] = character;

}

void Map::Welcome(){
	ifstream mapwelcome ("welcome.txt");

	string aux;

	for(int i = 0; i < 20; i++){
		getline(mapwelcome, aux);
		for(int j = 0; j < 50; j++){
			this->welcome[i][j] = aux[j];
			printw("%c", this->welcome[i][j]);
		}
		printw("\n");
	}

	mapwelcome.close();
}

void Map::GameOver(){
	ifstream maplose ("game_over.txt");

	string aux;

	for(int i = 0; i < 20; i++){
		getline(maplose, aux);
		for(int j = 0; j < 50; j++){
			this->gameOver[i][j] = aux[j];
			printw("%c", this->gameOver[i][j]);
		}
		printw("\n");
	}

	maplose.close();
}

void Map::Win(){
	ifstream mapwin ("win.txt");

	string aux;

	for(int i = 0; i < 20; i++){
		getline(mapwin, aux);
		for(int j = 0; j < 50; j++){
			this->win[i][j] = aux[j];
			printw("%c", this->win[i][j]);
		}
		printw("\n");
	}

	mapwin.close();
}

char Map::retornaElemento(int posx, int posy){
	return this->maze_new[posx][posy];
}
