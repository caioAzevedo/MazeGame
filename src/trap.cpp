#include <iostream>
#include <string>
#include "trap.hpp"
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>
#include <ctime>
#include <cstdlib>

using namespace std;


Trap::Trap(){
	this->character = 'T';
	this->posx = 0;
	this->posy = 0;
  this->damage = 1;
}

void Trap::setPosx(int valor){

	this->posx = valor;

}

void Trap::setPosy(int valor){
	this->posy = valor;
}

void Trap::setCharacter(char character){
	this->character = character;
}

void Trap::setDamage(int damage){
  this->damage = damage;
}

int Trap::getPosx(){
  return this->posx;
}

int Trap::getPosy(){
  return this->posy;
}

char Trap::getCharacter(){
  return this->character;
}

int Trap::getDamage(){
  return this->damage;
}

void Trap::movimento() {
  int x, y;
  x = rand() % 50;
  y = rand() % 20;
  this->posx = x;
  this->posy = y;
}


