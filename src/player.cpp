#include <iostream>
#include <string>
#include "player.hpp"
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>

using namespace std;


Player::Player(){
	this->character = '@';
	this->posx = 3;
	this->posy = 3;
	this->alive = 5;
	this->score = 0;
	this->winner = false;
}

void Player::setPosx(int valor){

	this->posx += valor;

}

void Player::setPosy(int valor){
	this->posy += valor;
}

void Player::setCharacter(char character){
	this->character = character;
}

void Player::setAlive(){
	this->alive--;
}

void Player::setScore(int valor){
	this->score += valor;
}

void Player::setWinner(bool win){
	this->winner = TRUE;
}

int Player::getPosx(){
	return this->posx;
}

int Player::getPosy(){
	return this->posy;
}

char Player::getCharacter(){
	return this->character;
}

int Player::getAlive(){
	return this->alive;
}

int Player::getScore(){
	return this->score;
}

bool Player::getWinner(){
	return this->winner;
}

void Player::movimento(char w, char s, char a, char d){

	char direcao = 'l';

	direcao = getch();

	if(direcao == 'w'){
		if(w == '='){}	else{
			this->setPosy(-1);
		}
	} else if (direcao == 's'){
		if(s == '='){}	else{
			this->setPosy(1);
		}
	} else if (direcao == 'a'){
		if(a == '='){}	else{
			this->setPosx(-1);
		}
	} else if (direcao == 'd'){
		if(d == '='){}	else{
			this->setPosx(1);
		}
	}

}




